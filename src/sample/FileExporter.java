package sample;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FileExporter {

    private PrintWriter writer;

    public FileExporter() {
    }

    public FileExporter(String name, FileType fileType, boolean timestamp) {
        try {
            if (timestamp) {
                Calendar cal = Calendar.getInstance();
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
                writer = new PrintWriter(name + " " + df.format(cal.getTime()) + fileType.getType(), "UTF-8");
            } else {
                writer = new PrintWriter(name + fileType.getType(), "UTF-8");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public PrintWriter getWriter() {
        return writer;
    }
}

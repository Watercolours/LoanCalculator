package sample;

public class ExcelColumn {

    private FileExporter xp;

    public ExcelColumn() {
        xp = new FileExporter("output", FileType.XLS, true);
    }

    public void multiDoubleOutput(String date, double... values) {
        try {
            xp.getWriter().print("\t" + date);
            for (double value : values) {
                xp.getWriter().print("\t" + value);
            }
            xp.getWriter().println();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void multiStringOutput(String... values) {
        try {
            xp.getWriter().print("\t");
            for (String value : values) {
                xp.getWriter().print("\t" + value);
            }
            xp.getWriter().println();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void newLine() {
        try {
            xp.getWriter().println();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void finish() {
        xp.getWriter().close();
    }
}

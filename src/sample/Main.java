package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception{
        stage.setTitle("Loan Calculator");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25));

        Scene scene = new Scene(grid, 650, 400);
        stage.setScene(scene);
        stage.show();

        Text title = new Text(":D");
        title.setFont(Font.font("Lucida Sans", FontWeight.NORMAL, 20));
        grid.add(title, 0, 0, 2, 1);

        grid.add(new Label("Loan:"), 0, 1);
        final TextField loan = new TextField();
        loan.setText("1500000");
        grid.add(loan, 1, 1);

        grid.add(new Label("Interest:"), 0, 2);
        final TextField interest = new TextField();
        interest.setText("3.5");
        grid.add(interest, 1, 2);

        grid.add(new Label("P.m.:"), 0, 3);
        final TextField perMonth = new TextField();
        perMonth.setText("15000");
        grid.add(perMonth, 1, 3);

        grid.add(new Label("Rent:"), 4, 1);
        final TextField rent = new TextField();
        rent.setText("2000");
        grid.add(rent, 5, 1);

        grid.add(new Label("Transportation:"), 4, 2);
        final TextField transportation = new TextField();
        transportation.setText("650");
        grid.add(transportation, 5, 2);

        grid.add(new Label("Additional Fees:"), 4, 3);
        final TextField additionalFees = new TextField();
        additionalFees.setText("50");
        grid.add(additionalFees, 5, 3);



        Button btn = new Button("Calculate");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                LoanValues lv = new LoanValues();

                lv.setTotalLoan(Double.parseDouble(loan.getText()));
                lv.setInterest(Double.parseDouble(interest.getText()));
                lv.setAdditionalFees(Double.parseDouble(additionalFees.getText()));
                lv.setPayPerMonth(Double.parseDouble(perMonth.getText()));
                lv.setRent(Double.parseDouble(rent.getText()));
                lv.setTransportationCost(Double.parseDouble(transportation.getText()));

                new Calculator().calculate(lv);
            }
        });
        grid.add(btn, 5, 4);
    }


    public static void main(String[] args) {
        launch(args);
    }
}

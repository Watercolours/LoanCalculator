package sample;

public enum FileType {
    TXT(".txt"),
    XLS(".xls"),
    DOC(".doc"),

    ;


    private final String type;

    FileType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
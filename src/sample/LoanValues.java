package sample;

public class LoanValues {

    private double totalLoan;
    private double remainingLoan;
    private double interest;
    private double payPerMonth;
    private double additionalFees;

    private double loanPerMonth;

    private double rent;
    private double transport;
    private double transport365;

    private double totalPaymentThisYear;
    private double totalLoanPaymentThisYear;
    private double totalInterestThisYear;
    private double totalRentThisYear;
    private double totalTransportThisYear;

    private int currentYear;


    public double getTotalLoan() {
        return totalLoan;
    }

    public void setTotalLoan(double totalLoan) {
        this.totalLoan = totalLoan;
        this.remainingLoan = totalLoan;
    }

    public double getRemainingLoan() {
        return remainingLoan;
    }

    public void setRemainingLoan() {
        remainingLoan -= getLoanPerMonth();
    }

    public double getPayPerMonth() {
        return payPerMonth;
    }

    public void setPayPerMonth(double payPerMonth) {
        this.payPerMonth = payPerMonth;
    }

    public double getLoanPerMonth() {
        return getPayPerMonth() - getInterestPerMonth();
    }

    public void setLoanPerMonth() {
        this.loanPerMonth = getPayPerMonth() - getInterestPerMonth();
    }

    public double getInterestPerMonth() {
        return ((getRemainingLoan() * (getInterest() / 100)) / 12) + getAdditionalFees();
    }

    public double getTotalPaymentThisYear() {
        return totalPaymentThisYear;
    }

    public void setTotalPaymentThisYear() {
        totalPaymentThisYear += getPayPerMonth();
    }

    public double getTotalLoanPaymentThisYear() {
        return totalLoanPaymentThisYear;
    }

    public void setTotalLoanPaymentThisYear() {
        totalLoanPaymentThisYear += getLoanPerMonth();
    }

    public double getTotalInterestThisYear() {
        return totalInterestThisYear;
    }

    public void setTotalInterestThisYear() {
        totalInterestThisYear += getInterestPerMonth();
    }

    public double getTotalRentThisYear() {
        return totalRentThisYear;
    }

    public void setTotalRentThisYear() {
        totalRentThisYear += getRent();
    }

    public double getTotalTransportThisYear() {
        return totalTransportThisYear;
    }

    public void setTotalTransportThisYear() {
        totalTransportThisYear += getTransportPerMonth();
    }

    public double getRent() {
        return rent;
    }

    public void setRent(double rent) {
        this.rent = rent;
    }

    public double getTransportPerMonth() {
        return transport;
    }

    public void setTransportationCost(double transport) {
        this.transport = transport;
    }

    public double getTransport365() {
        return transport365;
    }

    public void setTransport365(double transport365) {
        this.transport365 = transport365;
    }

    public int getCurrentYear() {
        return currentYear;
    }

    public void setCurrentYear(int currentYear) {
        this.currentYear = currentYear;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public double getAdditionalFees() {
        return additionalFees;
    }

    public void setAdditionalFees(double additionalFees) {
        this.additionalFees = additionalFees;
    }

    public void resetYearlyCosts() {
        totalInterestThisYear = 0;
        totalLoanPaymentThisYear = 0;
        totalPaymentThisYear = 0;
        totalRentThisYear = 0;
        totalTransportThisYear = 0;
    }

}

package sample;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Calculator {

    private LoanValues loan;

    public Calculator() {

    }

    protected void calculate(LoanValues loan) {
        this.loan = loan;

        ExcelColumn exporter = new ExcelColumn();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2014, 5, 1);
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        DecimalFormat df = new DecimalFormat("#.00");

        loan.setCurrentYear(calendar.get(Calendar.YEAR));

        while (loan.getRemainingLoan() > 0) {

            loan.setLoanPerMonth();
            loan.setRemainingLoan();
            loan.setTotalRentThisYear();
            loan.setTotalTransportThisYear();

            // totals
            loan.setTotalPaymentThisYear();
            loan.setTotalLoanPaymentThisYear();
            loan.setTotalInterestThisYear();

//            //output
            System.out.printf("%15s | %15.2f | %15.2f | %15.2f | %15.2f\n",
                    dateFormat.format(calendar.getTime()), loan.getRemainingLoan(),
                    loan.getPayPerMonth(), loan.getLoanPerMonth(), loan.getInterestPerMonth());

            exporter.multiDoubleOutput(dateFormat.format(calendar.getTime()),
                    loan.getRemainingLoan(),
                    loan.getPayPerMonth(),
                    loan.getLoanPerMonth(),
                    loan.getInterestPerMonth(),
                    loan.getTransportPerMonth()
            );


            calendar.add(Calendar.MONTH, 1);
//
            if (calendar.get(Calendar.YEAR) != loan.getCurrentYear()) {
                exporter.newLine();
                exporter.multiDoubleOutput("",
                        loan.getRemainingLoan(),
                        loan.getTotalPaymentThisYear(),
                        loan.getTotalLoanPaymentThisYear(),
                        loan.getTotalInterestThisYear(),
                        loan.getTotalTransportThisYear()
                );
                exporter.newLine();

                loan.setCurrentYear(calendar.get(Calendar.YEAR));
                loan.resetYearlyCosts();
            }
        }

        exporter.finish();
    }
}
